# Flask Celery example

在 Ubuntu 系统安装 Redi 可以使用以下命令:
```
$sudo apt-get update 
$sudo apt-get install redis-server
```
启动 Redis
```
$ redis-server
```
查看 redis 是否启动？
```
$ redis-cli
```
安装依赖
```
pip install redis
pip install flask
pip install celery

```
启动Celery app就是app.py
```
celery -A app worker --loglevel=info
```
测试
```
$ python3
>>>from app import add
>>>res = add.delay(4, 3)
```

